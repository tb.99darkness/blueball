﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private PlayerController player;
    [SerializeField]
    private float focus;
    [SerializeField]
    private float normal;
    [SerializeField]
    private float ultraview;
    private Vector3 target;
    // Update is called once per frame
    void Update()
    {
        MoveCamera();
    }

    private void MoveCamera()
    {
        target = new Vector3(player.transform.position.x, player.transform.position.y, 0);
        if (player.transform.position.y >= 4 && player.transform.position.y <= 5)
        {
            target.z = ultraview;
            transform.position = Vector3.Slerp(transform.position, target, 2f * Time.deltaTime);
        }
        else if (player.transform.position.y >= 0 && player.transform.position.y <= 4)
        {
            target.z = normal;
            transform.position = Vector3.Slerp(transform.position, target, 2f * Time.deltaTime);
        }
        else if (player.transform.position.y >= -2 && player.transform.position.y <= 0)
        {
            target.z = focus;
            transform.position = Vector3.Slerp(transform.position, target, 2f * Time.deltaTime);
        }
        if (transform.position.y > 2.04f)
        {
            transform.position = new Vector3(transform.position.x, 2.04f, transform.position.z);
        }
    }
}
