﻿using System.Collections;
using UnityEngine;

public class GroundDetecter : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem smokeParticle;
    [SerializeField]
    private float particleDuration;
    [SerializeField]
    private PlayerController player;
    [SerializeField]
    private AudioSource jumpSound;
    [SerializeField]
    private AudioSource landSound;
    private WaitForSeconds wait;
    private void Start()
    {
        wait = new WaitForSeconds(particleDuration);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        jumpSound.Play();
        player.isGrounded = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(10))
        {
            smokeParticle.gameObject.SetActive(true);
            StartCoroutine(DisableParticle());
        }
        landSound.Play();
        player.isGrounded = true;
    }

    private IEnumerator DisableParticle()
    {
        yield return wait;
        smokeParticle.gameObject.SetActive(false);
    }
}
