﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CustomButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool IsPress;
    public void OnPointerDown(PointerEventData eventData)
    {
        IsPress = true;
        Debug.Log(IsPress);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        IsPress = false;
        Debug.Log(IsPress);
    }
}
