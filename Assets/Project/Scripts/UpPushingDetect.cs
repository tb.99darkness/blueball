﻿using UnityEngine;

public class UpPushingDetect : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer cubeSprite;
    [SerializeField]
    private Transform cube;
    private float time;
    private float timeTemp;

    private void Start()
    {
        time = 2f;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();
        Debug.Log(player.isMove);
        if (player.isMove)
        {
            if (timeTemp == 0)
            {
                timeTemp = Time.time;
            }
            if (Time.time - timeTemp >= time)
            {
                cube.position += Vector3.right * player.speed / 8 * Time.deltaTime;
                Debug.Log(cube.position);
            }

        }
        else
        {
            timeTemp = 0;
        }
    }

}
