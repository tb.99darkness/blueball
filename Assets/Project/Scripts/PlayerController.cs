﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public bool isGrounded;
    [SerializeField]
    private CustomButton leftButton;
    [SerializeField]
    private CustomButton rightButton;
    [SerializeField]
    private Rigidbody2D rigid;
    [HideInInspector]
    public bool isMove = false;
    [SerializeField]
    private Transform playerImage;

    void Update()
    {

    }
    void FixedUpdate()
    {
        if (leftButton.IsPress)
        {
            MoveLeft();
        }
        else if (rightButton.IsPress)
        {
            MoveRight();
        }
        else
        {
            isMove = false;
        }
    }
    public void MoveLeft()
    {
        isMove = true;
        transform.position += Vector3.left * speed * Time.deltaTime;
        playerImage.Rotate(0, 0, 0.5f * speed, Space.World);
    }
    public void MoveRight()
    {
        isMove = true;
        transform.position += Vector3.right * speed * Time.deltaTime;
        playerImage.Rotate(0, 0, -0.5f * speed, Space.World);
    }
    public void Jump()
    {
        if (isGrounded)
        {
            rigid.AddForce(Vector2.up * 10, ForceMode2D.Impulse);
        }
    }
}
